%%main
load('UMD_Actions_data.mat');
addpath('Grassmann_Projection')

%starting point
S1 = data{1}; 
P1 = S1*S1';
subplot(131),plot(S1(:,2),-S1(:,1)) %visualize shape
title('Starting Point');

%ending point
S2 = data{35}; 
P2 = S2*S2';
subplot(132),plot(S2(:,2),-S2(:,1)) %visualize shape
title('Ending Point');


%%--------------------------------------------------------------%%
% Mapping to the tangent plane at P1
V = findVelocity_A_P1toP2(P1,P2); 
[u1 a1]= phi_inv(P1); 

%Time you want to travel along the veclocity vector V, starting from P1.
%If you try different values you will see different shapes along the
% geodesic between P1 and P2
T = 1; 


%mapping back to the manifold
tmp1 = findP2_from_P1_A(P1,V,u1,T); 

 %obtaining the shape representation from the 'P' form.
[um M] = phi_inv(tmp1);

%Obtaining the shape
S2_hat = M;
subplot(133),plot(S2_hat(:,2),-S2_hat(:,1)) %visualize shape