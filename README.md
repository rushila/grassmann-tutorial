####README#######

Playing with shapes as points on a Grassmann manifold. 

This is a simple toolbox to load, and play with shape data from the UMD actions dataset, after processing them to be points on the Grassmannian. It will give you some intuition into how shapes change, and playing with the exponential and log maps to gain some hands on experience with applied differential geometry. 

There are a total of 8000 shapes in the dataset (UMD_Actions_data.mat). The shapes are modeled as points on the Grassmann manifold, which provides affine invariance in addition to size and rotation invariance. 

When you run main.m, it will load two shapes from the dataset, find the tangent that takes one shape to another. The time one should travel along the tangent determines the shape that is output. This can be controlled by the parameter 'T' in main.m. The default value is T = 1, for 0<T<1 you can obtain intermediate shapes between the two original shapes. 

Thanks. 

Rushil

ranirudh@asu.edu